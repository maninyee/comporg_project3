We first use the command:

	g++ -Wall --std=c++11 -o lru-config1 example/slru.cc lib/config1.a

changing the config files to config2 when config2 testing is needed, and slru.cc to lru.cc when testing the LRU algorithm.



To get the output from the algorithms, we sent the output to a text file with the following command:

	./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/bzip2_10M.trace.gz >~/work/slru_out.txt

changing "~/work/slru_out.txt" to the name of the designated output file to save to, and config1 to config2 when testing for config2. We also change the argument for the -traces flag when testing for the other traces



We used these commands for each of the fies with the trace/bzip2_10M.trace.gz individually, but for all other traces we used a script named "runallconfigs.sh" to run the algorithms and save their output. Before starting this script we ran "g++ -Wall --std=c++11 -o lru-config1 example/slru.cc lib/config1.a" once more.



We manually placed the IPC counts from these text file outputs into a spreadsheet.
We then took the IPC's from the slru and lru test run for each config and compared them to each other to get the speed up of the algorithm we wrote.

Question 4:
To calculate the geometric mean of the processor speedup for one configuration, we multiplied all the speedups of one configuration then took the 5th root of that product. The spreadsheet equation for configuration 1 was “ = (B3*B4*B5*B6*B7)^(1/5),” and it equaled 1.00077565. For configuration 2, the spreadsheet equation was “ = (E3*E4*E5*E6*E7)^(1/5),“ and it equaled 0.99562315.
