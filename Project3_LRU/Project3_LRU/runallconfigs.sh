#!/bin/bash
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/graph_analytics_10M.trace.gz >sgraph1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/libquantum_10M.trace.gz >slibquantum1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/mcf_10M.trace.gz >smcf1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/xalancbmk_10M.trace.gz >sxalanc1
g++ -Wall --std=c++11 -o lru-config1 example/lru.cc lib/config1.a
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/graph_analytics_10M.trace.gz >lgraph1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/libquantum_10M.trace.gz >llibquantum1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/mcf_10M.trace.gz >lmcf1
./lru-config1 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/xalancbmk_10M.trace.gz >lxalanc1
g++ -Wall --std=c++11 -o lru-config2 example/slru.cc lib/config2.a
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/graph_analytics_10M.trace.gz >sgraph2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/libquantum_10M.trace.gz >slibquantum2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/mcf_10M.trace.gz >smcf2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/xalancbmk_10M.trace.gz >sxalanc2
g++ -Wall --std=c++11 -o lru-config2 example/lru.cc lib/config2.a
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/graph_analytics_10M.trace.gz >lgraph2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/libquantum_10M.trace.gz >llibquantum2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/mcf_10M.trace.gz >lmcf2
./lru-config2 -warmup_instructions 1000000 -simulation_instructions 10000000 -traces trace/xalancbmk_10M.trace.gz >lxalanc2
