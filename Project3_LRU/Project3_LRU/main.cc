// #include "../inc/champim_crc2.h"
#include <string>
#include <iostream>

//1536 in the probationary segment
//512 in the protected segments

#define NUM_CORE 1
#define LLC_SETS NUM_CORE*2048
#define LLC_WAYS 16
LRUClass lru[LLC_SETS][LLC_WAYS];

class LRUClass {
	uint32_t rank = 0;
	int numHits = 0;
};

class SetAndWay {
public:
	uint32_t set = 0;
	uint32_t way = 0;
};

// Given
void InitReplacementState() {
	for (int i = 0; i<LLC_SETS; i++) {
		for (int j = 0; j<LLC_WAYS; j++) {
			lru[i][j] = j;
		}
	}
}

SetAndWay GetVictimInSet(uint32_t cpu, uint32_t set, const BLOCK *current_set, uint64_t PC, uint64_t paddr, uint32_t type) {
	SetAndWay temporary;
	for (int i = 0; i < LLC_WAYS; i++) {
		if (lru[set][i] == (LLC_WAYS - 1)) {
			temporary.set = set;
			temporary.way = i;
			return temporary;
		}
	}
}

void UpdateReplacementState(uint32_t cpu, uint32_t set, uint32_t way, uint64_t paddr, uint64_t PC, uint64_t victim_addr, uint32_t type, uint8_t hit) {
	for (int i = 0; i<LLC_WAYS; i++) {
		if (lru[set][i] < lru[set][way]) {
			lru[set][i]++;
		}
	}
	lru[set][way].rank = 0; // promote to MRU
}

void ToProtected(uint32_t probset, uint32_t protset, uint32_t way) {
	if (lru[probset][way].numHits >= 2) {	// If accessed twice or more
		for (int i = 1536; i < LLC_SETS; i++)	//If there is space in protected	//refers to the protected segment
		{
			for (int j = 0; j < LLC_WAYS; j++)//moves through every way in the set
			{
				if (lru[i][j] == NULL)	//if the protected segment has an empty way
				{
					lru[i][j] == lru[probset][way];	//the protected segments way becomes what was in the probationary segment
					lru[probset][way] == NULL;		//the probationary set's way is now empty
					LRUClass temp.numHits = numHits++;
					return;		//breaks out of the for loop
				}
			}
		}
		SetAndWay newSetAndWay = GetVictimInSet(1, );	// Get victim from protected set and store the way
		for (int i = 0; i < 1535; i++)	//If protected is full	//refers to the protected segment
		{
			for (int j = 0; j < LLC_WAYS; j++)	//moves through every way in the probationary set
			{
				if (lru[i][j] == NULL)	//if the probationary segment has an empty way
				{
					lru[i][j] == lru[newSetAndWay.set][newSetAndWay.way];	//the protected segments way becomes what was in the probationary segment
					lru[probset][way] == NULL;		//the probationary sets way is now empty
					LRUClass temp.numHits = numHits++;  // Increment hits
					return;		//breaks out of the for loop
				}
			}
		}
		SetAndWay victim = GetVictimInSet(1, );	// Get victim in probationary set
		lru[victim.set][victim.way] = lru[newSetAndWay.set][newSetAndWay.way];	// Get rid of that victim and set it to the new value
		UpdateReplacementState();	// Update
	}
}

void ToProbationary(uint32_t probset, uint32_t way) {
	for (int i = 0; i < 1535; i++) {
		for (int j = 0; j < LLC_WAYS; j++) {
			if (lru[i][j] == NULL)	//if the probationary segment has an empty way
				{
					lru[i][j] == lru[probset][way];	// Put here
					LRUClass temp.numHits = numHits++;  // Increment hits
					return;	
				}
		}
	}
	SetAndWay victim = GetVictimInSet(1, );	// Get victim in probationary set if full
	lru[victim.set][victim.way] = lru[probset][way];	// Get rid of that victim and set it to the new value
	LRUClass temp.numHits = numHits++;  // Increment hits
	UpdateReplacementState();	// Update
	
}

int main() {
	// get value
    // check if value is in probationary
        // if in probationary already call to protected
        // else if check protected
            // if in protected, call to protected
        // else call to probationary
    return 0;
}
